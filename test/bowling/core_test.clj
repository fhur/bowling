(ns bowling.core-test
  (:require [clojure.test :refer [are deftest testing]]
            [bowling.parser :as parser]
            [bowling.core :refer :all]))

(deftest test-parse-and-calc-score
  (testing "Parsing and calculating the score should yield the correct result"
    (are [score game-string]
      (= score (calculate-game-score (parser/parse-game game-string)))
      0   "| 0 0 | 0 0 | 0 0 | 0 0 | 0 0 | 0 0 | 0 0 | 0 0 | 0 0 | 0 0 |"
      20  "| 1 1 | 1 1 | 1 1 | 1 1 | 1 1 | 1 1 | 1 1 | 1 1 | 1 1 | 1 1 |"
      20  "| 5 / | 5 0 |"
      5   "| 1 4 |"
      14  "| 1 4 | 4 5 |"
      24  "| 1 4 | 4 5 | 6 /"
      38  "| 6 / | 6 / | 6 0"
      72  "| 6 / | 5 / | 4 / | 3 / | X | 0 0 |"
      300 "| X | X | X | X | X | X | X | X | X | X X X|"
      299 "| X | X | X | X | X | X | X | X | X | X X 9|"
      133 "| 1 4 | 4 5 | 6 / | 5 / | X | 0 1 | 7 / | 6 / | X | 2 / 6 |"
      125 "| 6 / | 4 / | 3 4 | 1 6 | 1 8 | X | 5 / | X | 9 / | 0 5 |"
      146 "| 7 / | 4 2 | 6 3 | 8 1 | 9 / | 8 / | 8 / | 8 / | 9 / | 5 / X |")))

