(ns bowling.core
  (:require
    [clojure.string :as string]
    [clojure.spec.alpha :as s]
    [bowling.parser :as parser])
  (:gen-class))


(defn flatten-throws
  "Returns a lazy sequence of throws starting from the given frame."
  [frames]
  (if-let [frame (first frames)]
    (lazy-cat (:throws frame)
              (flatten-throws (rest frames)))
    []))


(defn bonus
  "Calculate the bonus for a given frame by adding the result of the next n throws
  where n is the `num-throws` parameter."
  [num-throws frames]
  (->> (flatten-throws frames)
       (take num-throws)
       (reduce +)))


(defn calculate-game-score
  "Takes a game as input and returns the game's score."
  [frames]
  (loop [; the remaining unscored frames
         remaining-frames frames
         ; the amount to be returned
         score 0]
    (if (empty? remaining-frames)
      score
      (let [{:keys [throws carry]} (first remaining-frames)
            tail (rest remaining-frames)
            frame-score (reduce + throws)
            frame-bonus (bonus carry tail)]
        (recur tail
               (+ score frame-score frame-bonus))))))


(defn -main [& args]
  (let [file-path (first args)]
    (println "Loading:" file-path)
    (->> file-path
         (slurp)
         (parser/parse-game)
         (calculate-game-score)
         (println))))
