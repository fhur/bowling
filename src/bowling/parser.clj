(ns bowling.parser
  "A namespace that encapsulates all parsing code"
  (:require [clojure.string :as string]))

;; ---- constants ----

(def +frames-per-game+
  "The number of frames in a bowling game"
  10)

(def +score-per-strike+
  "The number of points per strike"
  10)

(def +carry-per-strike+
  "The number of additional throws that will contain a bonus when the player scores a strike"
  2)

(def +carry-per-spare+
  "The number of additional throws that will contain a bonus when the player scores a spare"
  1)

;; ---- Parsers ----
;; A parser is a pair of two functions (matcher, transformer). The matcher is a predicate
;; that takes a string as input and returns true iff the parser recognizes the given string.
;; The transformer takes the previous (possibly nil) and currently matched strings and returns
;; the parsed/transformed value.

(def strike-parser
  "Accepts and parses the strikes (X)"
  {:matcher     #{"X"}
   :transformer (fn [_prev-char _curr-char]
                  {:throw +score-per-strike+
                   :carry +carry-per-strike+})})

(def spare-parser
  "Accepts and parses the spares (/)"
  {:matcher     #{"/"}
   :transformer (fn [prev-char _curr-char]
                  {:throw (- +frames-per-game+ (Integer/parseInt prev-char))
                   :carry +carry-per-spare+})})

(def digit-parser
  "Accepts and parses digits (e.g. 5)"
  {:matcher     #(re-find #"^\d$" %)
   :transformer (fn [_prev-char curr-char]
                  {:throw (Integer/parseInt curr-char)
                   :carry 0})})

(def catch-all-parser
  "Accepts anything and always throws an exception. This is meant to always be the last parser as a
  crude form of error reporting."
  {:matcher     (fn [_] true)
   :transformer (fn [prev-char curr-char]
                  (throw (ex-info (str "Unable to parser char " curr-char) [prev-char curr-char])))})

;; ---- end parsers ----

(def parsers
  "A vector of all parsers."
  [strike-parser spare-parser digit-parser catch-all-parser])

(defn- find-first-matching-parser
  [throw-string]
  (->> parsers
       (filter (fn [parser] ((:matcher parser) throw-string)))
       (first)))

(defn- parse-frame
  [index frame-string]
  (let [throws (string/split frame-string #" ")
        ; map the results obtained from each parser
        parse-results (map-indexed (fn [i curr-char]
                                     (let [; the previous char or nil if there is none.
                                           prev-char (nth (cons nil throws) i)
                                           transformer (:transformer (find-first-matching-parser curr-char))]
                                       (transformer prev-char curr-char)))
                                   throws)
        ; obtain the parsed throws.
        throw-nums (vec (map :throw parse-results))
        ; calculate the carry by choosing the max carry in the current frame.
        carries (map :carry parse-results)
        is-last-frame? (= index (dec +frames-per-game+))]
    {:throws throw-nums
     :carry  (if is-last-frame? 0 (apply max carries))}))


(defn parse-game
  "Takes a string as input and returns a game object."
  [game-string]
  (->> (string/split game-string #"\|")
       (map #(string/trim %))
       (filter (complement empty?))
       (map-indexed parse-frame)))


