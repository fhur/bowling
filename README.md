# bowling

The synple bowling challenge.

## Usage

#### Step 1: build

To build you will need Leiningen 2.7.1. This is my exact version string:

```bash
Leiningen 2.7.1 on Java 1.8.0_92 Java HotSpot(TM) 64-Bit Server VM
```

Once you have Leiningen installed run the following:

```bash
lein compile && lein uberjar
```

#### Step 2: run

```bash
java -jar target/bowling-0.1.0-SNAPSHOT-standalone.jar <YOUR_FILE>
```

Where `<YOUR_FILE>` should be replaced with the bowling file. Example:

```bash
java -jar target/bowling-0.1.0-SNAPSHOT-standalone.jar input.txt
```
